# git transfert de fichiers
  
 **Méthode 1 via Webstorm**

 1. Dans le dossier racine du projet entrer la commande git init 
 2. Vérifier qu'un icône bleu et un icone verte apparaît en surbrillance en haut à droite de l'écran et les fichier apparaissant en rouge ne sont pas identifiés par git.
 3. Pour exécuter un git add sur le fichier sélectionner le fichier correspondant  avec un  clic droit puis une fois que le menu s'affiche  aller sur git -> add le fichier passe au vert.
 4. Cliquer sur l'icone vert en surbrillance décocher les cases dans le menu Git (perform code analysis,Check Todo)
 5. Ecrire un message dans la zone de commit afin d'apporter des infos à l'équipe de développement.
 6. Renseigner l'url du repo dans le define remote
 7. Cliquer sur commit and push

 **Méthode 2  en ligne de commande**
  

 1.  Dans le dossier racine du projet entrer la commande git init 
 2.  git remote add origin <url depôt>
 3. git add .
 4. git commit -m "commit message"
 5. git push -u origin master

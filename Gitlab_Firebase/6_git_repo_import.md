# git import repo
  
 **Méthode 1 via Webstorm**

 1. Aller dans le menu VCS -> checkout from version control -> Git  une fenêtre clone repository doit apparaître.
 2. Renseigner l'url  du repo dans le champ URL
 3. Cliquer sur clone
 4. Une fenetre proposant de d'ouvrir le dossier téléchargé opter ouvrir le fichier si on veut travailler tout de suite dessus sinon le dossier sera stocke dans l'adresse renseigné avant  dans le champ location de la fenetre clone repository


 **Méthode 2  en ligne de commande**
  

 1. Accéder dans le dossier ou vous voulez stocker le projet
 2.  git clone  [url repo]

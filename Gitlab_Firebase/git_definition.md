# git
  
 **Définition**

**Git** est un logiciel qui permet de stocker un ensemble de fichiers en conservant la chronologie de toutes les modifications qui ont été effectuées dessus. Il permet notamment de retrouver les différentes versions d'un lot de fichiers connexes.. C'est un logiciel libre créé par Linus Torvalds, auteur du noyau Linux , et distribué selon les termes de la licence publique générale GNU version 2. En 2016, il s’agit du logiciel de gestion de versions le plus populaire qui est utilisé par plus de douze millions de personnes

    
 **Fonctionnement**
Git possède deux  structures de données : une base d'objets et un cache de  répertoires. Il existe cinq types d'objets :

-   l'objet  **blob**  (pour  binary large object désignant un ensemble de données brutes 7, qui représente le contenu d'un  fichier  ;
-   l'objet  **tree**  (mot anglais signifiant arbre), qui décrit une arborescence de fichiers. Il est constitué d'une liste d'objets de type  _blob_s et des informations qui leur sont associées, tel que le nom du fichier et les  permissions. Il peut contenir  récursivement   d'autres  _trees_  pour représenter les sous-répertoires ;
-   l'objet  **commit**  (résultat de l'opération du même nom signifiant « valider une transaction , qui correspond à une arborescence de fichiers (_tree_) enrichie de métadonnées comme un message de description, le nom de l'auteur, etc.  Il pointe également vers un ou plusieurs objets  _commit_  parents pour former un graphe d'historiques.
-   l'objet  **tag**  (étiquette) qui est une manière de nommer arbitrairement un commit spécifique pour l'identifier plus facilement. Il est en général utilisé pour marquer certains commits, par exemple par un numéro ou un nom de version (2.1 ou bien  Lucid );
-   l'objet  **branch**  (branche) qui contient une partie de l'avancement du projet. Les branches sont souvent utilisées pour avancer dans une partie du projet sans impacter une autre partie.

La base des objets peut contenir n'importe quel type d'objets. Une couche intermédiaire, utilisant des index (les sommes de contrôle), établit un lien entre les objets de la base et l'arborescence des fichiers.

Chaque objet est identifié par une somme de contrôle  SHA-1 de son contenu. Git calcule la somme de contrôle et utilise cette valeur pour déterminer le nom de fichier de l'objet. L'objet est placé dans un répertoire dont le nom correspond aux deux premières lettres de la somme de contrôle. Le reste de la somme de contrôle constitue alors le nom du fichier pour cet objet.

Git enregistre chaque révision dans un fichier en tant qu'objet  _blob_  unique. Les relations entre les objets  _blob_s sont déterminées en examinant les objets  _commit_. En général, les objets  _blob_s sont stockés dans leur intégralité en utilisant la compression de la  zlib. Ce principe peut rapidement consommer une grande quantité de place disque ; de ce fait, les objets peuvent être combinés dans des archives, qui utilisent la compression différentielle (c'est-à-dire que les  _blob_s sont enregistrés sous la forme de différences par rapport aux autres  _blob_s).
 


**Quelques commandes**
Git dispose notamment des commandes suivantes :

-   `git init`  crée un nouveau dépôt ;
-   `git clone`  clone un dépôt distant ;
-   `git add`  ajoute de nouveaux objets  _blob_s dans la base des objets pour chaque fichier modifié depuis le dernier  _commit_. Les objets précédents restent inchangés ;
-   `git commit`  intègre la somme de contrôle  [SHA-1](https://fr.wikipedia.org/wiki/SHA-1 "SHA-1")  d'un objet  _tree_  et les sommes de contrôle des objets  _commit_s parents pour créer un nouvel objet  _commit_ ;
-   `git branch`  liste les branches ;
-   `git merge`  fusionne une branche dans une autre ;
-   `git rebase`  déplace les commits de la branche courante devant les nouveaux commits d’une autre branche ;
-   `git log`  affiche la liste des commits effectués sur une branche ;
-   `git push`  publie les nouvelles révisions sur le  _remote_. (La commande prend différents paramètres) ;
-   `git pull`  récupère les dernières modifications distantes du projet (depuis le  _Remote_) et les fusionne dans la branche courante ;
-   `git stash`  stocke de côté un état non commité afin d’effectuer d’autres tâches.
**Sites d'hébergement**
Il existe différents sites (également appelées  _forges_) généralistes d'hébergement, ainsi que des sites dédiés à des développements en particulier, acceptant des projets liés d'utilisateur, comme l'instance cgit de Kernel.org  ou les instances Gitlab de  Freedesktop.org,  Gnome,  KDE  ou  Freedesktop.org, ou encore de  Blender.
-   Framagit, site communautaire associatif libre de  Framasoft, basé sur  Gitlab.
-   GitHub est un service web d'hébergement et de gestion de développement de logiciels développé en  Ruby on Rails  et  Erlang , qui appartient à  Microsoft   depuis  juin 2018 ;
-   GitLab.com équivalent à GitHub et développé en  Ruby, évolution du code du service  Gitoriousracheté en  mars 2015, le code source est disponible et utilisé par différentes autres forges ;
-   GNU Savannah, permettant également l'utilisation de Mercurial, dépôt officiel de la  Free Software  ;
-   SourceForge.net , probablement le plus ancien, ouvert en 1999, il utilisait  CVS à ses débuts, avant de passer à  Subversion  (SVN), puis récemment à Git également. Il utilise aujourd'hui Apache Allura
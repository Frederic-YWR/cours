# Markdown
  
   **Définition**
   
   __Markdown__ :   Markdown est un langage de balisage léger créé en 2004 par John Gruber avec l'aide d'Aaron Swartz. Son but est d'offrir une syntaxe facile à lire et à écrire. Un document balisé par Markdown peut être lu en l'état sans donner l’impression d'avoir été balisé ou formaté par des instructions particulières.

Un document balisé par Markdown peut être converti en HTML, en PDF ou en d'autres formats. Bien que la syntaxe Markdown ait été influencée par plusieurs filtres de conversion de texte existants vers HTML —
 dont Setext, atx, Textile, reStructuredText, Grutatext et EtText —, la source d’inspiration principale est le format du courrier électronique en mode texte.(cf Wikipédia)

**Utilisation**
Le langage de balisage léger Markdown est utilisé dans de nombreuses applications, que ce soit des logiciels/éditeurs de code, des logiciels d'écriture, des plateformes de code, ou encore des chaînes d'édition avec l'addition d'autres logiciels. Les domaines concernés peuvent être le développement informatique (Markdown est habituellement utilisé pour formater le fichier README décrivant le code source d'un programme), la rédaction de documentation technique ou encore la publication académique. Markdown est probablement le langage de balisage léger le plus utilisé pour produire des documents numériques.(cf Wikipédia)

 
 **Syntaxe (Liste non exhaustive)**
 
 - `#`:Titre de niveau 1 (equivalent HTML : `<h1>`)
 - `##`:Titre de niveau 2 (equivalent HTML : `<h2>`)
 - `texte`: paragraphe (equivalent HTML : `<h2>`)
 - `*texte*`: emphase (equivalent HTML :`<em>`)
 - `**texte**`: emphase (equivalent HTML : `<h4>`)
 - `[texte du lien](url_du_lien "texte pour le titre, facultatif")`: lien (equivalent HTML : `<a>`)
 - `![Texte alternatif](url_de_l'image "texte pour le titre, facultatif")`: lien (equivalent HTML : `<img>`)
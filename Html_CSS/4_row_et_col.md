# grid (bootstrap)
  
 **Définition**

Le système de grille de Bootstrap est construit avec FlexBox et permet jusqu'à 12 colonnes sur la page.

Le système de grille Bootstrap 4 a cinq classes:
   l = largeur
-   `.col-` (Petits appareils supplémentaires -  l < à    576px)
-   `.col-sm-` (Petits appareils -  576px <= l <=768px )
-   `.col-md-` ( moyen -  768px <= l <= 992px)
-   `.col-lg-` (Gros appareils -  992px  <= l <= 1200px )
-   `.col-xl-` (Dispositifs de XLarge -  >= 1200px)

    
 **Structure de Base**
```
<!--Définir la taille des colonnes et comment elle va apparaitre sur différents appareils-->  
<div class="row">  
<div class="col-(SM, MD, lg ou xl)-(numéro entre 1 et 12)"></div>  
<div class="col-(SM, MD, lg ou xl)-(numéro entre 1 et 12)"></div>  
</div>  
<div class="row">  
<div class="col-(SM, MD, lg ou xl)-(numéro entre 1 et 12)"></div>  
<div class="col-(SM, MD, lg ou xl)-(numéro entre 1 et 12)"></div>  
<div class="col-(SM, MD, lg ou xl)-(numéro entre 1 et 12)"></div>  
</div>  
  
<!-- Reglage auto par bootstrap -->  
<div class="row">  
<div class="col"></div>  
<div class="col"></div>  
<div class="col"></div>  
</div>
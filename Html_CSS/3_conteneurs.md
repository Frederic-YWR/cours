# conteneurs (bootstrap)
  
 **Définition**

En **HTML**, un **container** est un élément qui peut contenir d'autres éléments, par exemple **<div>, <span>**, .. La classe **.container** ou la classe **.container-fluid** peuvent utiliser ces éléments.

### Classe .container-fluid

 L'application de la classe **.container-fluid** à un élément fait que la largeur de cet élément ait 100%.

    

```
<!DOCTYPE html>
<html>
<head>
  <title>exemple container-fluid</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>
<body>
  
    <div class="container-fluid">
  
      <!--Inserer le contenu dans le container-->
      
     </div>

</body>
</html>
```
### Class .container

Basé sur la taille de largeur de l'écran des appareils,  **Bootstrap**  les classifie en 5 classes :

1.  Les appreils dont la largeur est inférieure à  **567px**  sont considérés comme étant  **Extra Small** (très petits).
2.  Les appareils dont la largeur est supérieure ou égale à  **567px**  sont considérés comme  **Small** (petits) ou appelés équipements  **sm**.
3.  Les appareils dont la largeur est supérieure ou égale à  **768px** sont considérés comme  **Medium** (Moyen) ou appelés équipements  **sd**.
4.  Les appareils dont la largeur est supérieure ou égale à  **992px** considérés comme  **Large** (Grand) ou appelés équipements  **lg**.
5.  Les appareils dont la largeur est supérieure ou égale à **1200px**  considérés comme **Extra Large**  (Très grand), ou appelés équipements **xl**.

Selon la taille de l'image l'utilisation de la classe container aura comme effet:
     

 1. Si la largeur de l'équipement est inférieur à **567px**, la largeur de l'élément sera à 100%.
 2. Si la largeur de l'équipement est supérieure ou égale à **567px** et inférieure à **768px** l'élément affichera au centre (center) et sa largeur est à **567px**.
 3. Si la largeur de l'équipement est supérieure ou égale à **768px** et inférieure à **992px**, l'élément affichera au centre (center) et sa largeur est à **768px**
 4. Si la largeur de l'équipement est supérieur ou égale à **992px** et inférieure à **1200px**, l'élément affichera au centre (center) et la largeur de cet élément est **992px**.
 5.  Si la largeur de l'équipement est supérieur ou égale à **1200px**, l'élément affichera au centre (center) et la largeur de cet élément est **1200px**.

   ```<!DOCTYPE html>
<html>
<head>
  <title>exemple container</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>
<body>
  
    <div class="container">
  
      <!--Inserer le contenu dans le container-->
      
     </div>

</body>
</html>
 ```
# Méthodes pour arrays
  
**Array.Indexof(item, start)**

Valeur des arguments :

 - item(obligatoire) : l'élément à rechercher
 - start(facultatif) : index de l'array ou la méthode commence sa recherche.

La méthode indexOf () recherche dans le tableau l'élément spécifié et renvoie sa position.

La recherche commencera à la position spécifiée, ou au début si aucune position de début n'est spécifiée, et terminera la recherche à la fin du tableau.

Renvoie -1 si l'élément n'est pas trouvé.

Si l'élément est présent plusieurs fois, la méthode indexOf renvoie la position de la première occurrence.



 **Array.toString()**

La méthode JavaScript `toString()`convertit un tableau en une chaîne de caractères des valeurs de ce même  tableau (séparées par des virgules).

    let animaux = ["loup","zèbre","chien","lion","renard","faon"];
    console.log(animaux.toString());
    //renvoie "loup,zèbre,chien,lion,renard,faon"
    
 **Array.join(separator)**
 
  Valeur des arguments :

 - separator(facultatif) : le séparateur à utiliser 
 
La méthode `join()` se comporte comme `toString()`, mais il est possible de spécifier le séparateur en argument

             
     let animaux = ["loup","zèbre","chien","lion","renard","faon"];
     console.log( animaux.join("*") );
    //renvoie  "loup*zèbre*"chien*lion*renard";
  

  
**Array.pop()**

La méthode ``pop() `` supprime le dernier élément d'un tableau:

    let animaux = ["loup","zèbre","chien","lion","renard","faon"];
    console.log( animaux.pop() );
    //renvoie  ["loup","zèbre","chien","lion","renard"]

**Array.push(item,item2,......,itemX)**

Valeur des arguments :

 - item,item2,......,itemX : les éléments à ajouter au tableau

La méthode  ``push()``ajoute un nouvel élément à la fin du tableau


    let animaux = ["loup","zèbre","chien","lion","renard","faon"];
    console.log( animaux.push("guèpe") );
    //renvoie  ["loup","zèbre","chien","lion","renard","faon","guèpe"];
    
    




**Array.shift()**

La méthode  ``shift()``retire le premier élément du tableau et retourne la valeur de cet élément

    let animaux = ["loup","zèbre","chien","lion","renard","faon"];
    let value = animaux.shift();
    console.log(value);
    // Renvoie "loup"
    console.log(animaux);
    // Renvoie ["zèbre","chien","lion","renard","faon"];
    
  **Array.unshift(item1,item2, ..., itemX)** 

La méthode  ``unshift()``ajoute un nouvel élément en début du tableau et retourne la longueur du nouveau tableau

    let animaux = ["loup","zèbre","chien","lion","renard","faon"];
    let value = animaux.unshift("léopard");
    console.log(value);
    // Renvoie 7
    console.log(animaux);
    //  Renvoie ["léopard","zèbre","chien","lion","renard","faon"];
    
  Valeur des arguments :

 - item,item2,......,itemX : les éléments à ajouter au tableau  
    
**Array.splice(index,items_to_remove, item_to_add1,  item_to_add2,....., item_to_addX)**  

Valeur des arguments :

 - index : index de l'array ou la méthode est censée exécuter sa fonction  
 - items_to_remove : nombre éléments à supprimer 
 - item_to_add1,  item_to_add2,....., item_to_addX : les élements à ajouter

 La méthode splice () cette méthode modifie le tableau d'origine en  ajoutant ou supprimant des éléments vers / depuis un tableau et renvoie les éléments supprimés.
  
     
    let animaux = ["loup","zèbre","chien","lion","renard","faon"];
    let value   = animaux.splice(2, 1, "éléphant","hyène")
    console.log(value);
    // Renvoie "chien"
     console.log(animaux);
     // Renvoie ["loup","zèbre","éléphant","hyène","lion","renard","faon"]
    


 

    
**Array.slice(start,end)** 

Valeur des arguments :

 - start  : début de l'échantillon
 - end    : fin de l'échantillon

La méthode slice () renvoie les éléments sélectionnés dans un tableau, en tant que nouvel objet tableau.

La méthode slice () sélectionne les éléments commençant à l' argument de début donné , et se termine à, mais n'inclut pas , l' argument de fin donné .

    let animaux = ["loup","zèbre","chien","lion","renard","faon"]
    let value = fruits.slice(1, 3);
    console.log("value");
    //Renvoie ["zèbre","chien"];
    


**Array.concat(array1, _array2, ..., _arrayX)** 

Valeur des arguments :

 - array1, _array2, ..., _arrayX : les arrays à fusionner
La méthode `concat()`est utilisée pour joindre deux ou plusieurs tableaux.

Cette méthode ne modifie pas les tableaux existants, mais renvoie un nouveau tableau, contenant les valeurs des tableaux joints.

    let girls = ["Cecile", "Lisa"];
    let boys = ["Emile", "Thomas", "Lionel"];
    let children = boys.concat(girls);
    console.log(children)
    //Renvoie ["Cecile", "Lisa","Emile", "Thomas", "Lionel"]
# Arrays 
  
 **Définition**

Les tableaux sont généralement décrits comme des "objets de type liste" ; un tableau est un objet contenant plusieurs valeurs. Les objets tableau peuvent être stockés dans des variables et traités de la même manière que tout autre type de valeur, la différence étant que nous pouvons accéder à chaque valeur du tableau individuellement, et faire des choses super utiles et efficaces avec la liste des valeurs, comme boucler et faire la même chose pour chaque valeur. Peut-être que nous avons une série d'articles et leurs prix stockés dans un tableau, et nous voulons les parcourir tous et les imprimer sur une facture, tout en totalisant tous les prix ensemble et en imprimant le prix total en bas.

Sans tableaux, nous devrions stocker chaque valeur dans une variable séparée, puis appeler le code qui effectue l'affichage ou l'impression, puis ajouter séparément chaque élément. Ce serait plus long à écrire, moins efficace et cela comporterait plus de risques d'erreurs.(cf MDN)


 **Création d'un array**
  

    let numbers = [1,2,3,4,5,6]


**Accéder aux éléments d'un array**

    numbers[0];
    // Renvoie 1
    
on peut énumérer les éléments d'un array à l'aide des index dont la premiere valeur commence par 0 :
 [0]  est le premier élément du tableau numbers,[5] est le dernier élément du tableau numbers .
    
**Modifier les  éléments d'un array**

    numbers[0] = 2;
    console.log("numbers");
    //Renvoie [2,2,3,4,5,6]
    
**Nombre d 'éléments d'un array**

La propriété length d'un array retourne le nombre d'éléments présents dans cet array

     let numbers = [1,2,3,4,5,6];
     console.log(numbers.length);
     // Renvoie 6
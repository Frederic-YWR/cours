# Connexion au NAS
  
   **Définition**
   
   __NAS__ : Un serveur de stockage en réseau, également appelé stockage en réseau NAS, boîtier de stockage en réseau ou plus simplement NAS (de l'anglais Network Attached Storage), est un serveur de fichiers autonome, relié à un réseau, dont la principale fonction est le stockage de données en un volume centralisé pour des clients réseau hétérogènes.(cf Wikipédia)

**Connexion**

La connexion au NAS  se fait avec un navigateur il faut :

 - Se connecter sur l'adresse IP du serveur
 - Valider l'authentification (login et mot de passe)
 
 **Contenu**
 
 - Filestation : Système de fichiers ou figure les éléments nécessaires (fichiers et cours vidéos) pour suivre la formation.
 - Videostation : Dépôt des vidéos des cours
 
 **Mise en garde**

Par mesure de sécurité le serveur bannit le compte crée au bout de 10   
tentatives de connexions échouées.
Prévenir l'administrateur  en cas de 5 essais ratés.
 